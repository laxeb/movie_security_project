package org.polytech.fr.securite.utils;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import lombok.extern.slf4j.Slf4j;
import org.polytech.fr.securite.entity.Movie;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.polytech.fr.securite.utils.IndexationBD.*;

@Slf4j
public class Parseur {


    //Parser le fichier csv en un tableau
    public static List<Movie> parseMovies(String fileName) throws IOException, CsvException
    {
        // Read the file using CSVreader
        CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(fileName))
                .withCSVParser(csvParser)   // custom CSV parser
                .withSkipLines(1)           // skip the first line, header info
                .build()){

            List<String[]> r = reader.readAll();

            List<Movie> movies = r.stream()
                                          .filter( x -> x.length > 11)
                                          .map(line ->
                                               Movie.builder()
                                                       .showId(line[SHOW_ID.getIndex()])
                                                       .title(line[TITLE.getIndex()])
                                                       .cast(new ArrayList<>(List.of(line[CAST.getIndex()].split(","))))
                                                       .type(line[TYPE_MOVIE.getIndex()])
                                                       .country(line[COUNTRY.getIndex()])
                                                       .rating(line[RATING.getIndex()])
                                                       .description(line[DESCRIPTION.getIndex()])
                                                       .dateAdded(line[DATE_ADDED.getIndex()])
                                                       .duration(line[DURATION.getIndex()])
                                                       .director(new ArrayList<>(List.of(line[DIRECTOR.getIndex()].split(","))))
                                                       .releaseYear(line[REALEASED_YEAR.getIndex()])
                                                       .listedIn(new ArrayList<>(List.of(line[LIST_IN.getIndex()].split(","))))
                                                       .build()
                                              )
                                 .toList();
            return movies ;
        }
    }
}
