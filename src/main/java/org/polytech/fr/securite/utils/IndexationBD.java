package org.polytech.fr.securite.utils;

public enum IndexationBD {
    SHOW_ID(0),
    TYPE_MOVIE(1),
    TITLE(2),
    DIRECTOR(3) ,
    CAST(4),
    COUNTRY(5),
    DATE_ADDED(6),
    REALEASED_YEAR(7),
    RATING(8),
    DURATION(9),
    LIST_IN(10),
    DESCRIPTION(11) ;

    private int index ;

    private  IndexationBD(int val )
    {
        index = val ;
    }

    public int getIndex() {
        return index;
    }
}
