package org.polytech.fr.securite.controller;

import org.polytech.fr.securite.entity.RequestLogin;
import org.polytech.fr.securite.entity.UserProfile;
import org.polytech.fr.securite.firebase.DatabaseMovies;
import org.polytech.fr.securite.service.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authenticate")
public class MyAuth {

    private static final Logger LOG = LoggerFactory.getLogger(MyAuth.class);

    private final TokenService tokenService;
    private  final DatabaseMovies databaseMovies ;
    private final AuthenticationManager authenticationManager;

    private final BCryptPasswordEncoder passwordEncoder;

    public MyAuth(TokenService tokenService, DatabaseMovies databaseMovies, AuthenticationManager authenticationManager, BCryptPasswordEncoder passwordEncoder) {
        this.tokenService = tokenService;
        this.databaseMovies = databaseMovies;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;

    }

    @PostMapping("/token")
    public ResponseEntity<String> token(@RequestBody RequestLogin userLogin) throws AuthenticationException {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userLogin.username(),userLogin.password()));
        return ResponseEntity.ok()
                .body( tokenService.generateToken(authentication));

    }

    @PostMapping("add/user")
    public  void signin (@RequestBody UserProfile userProfile) throws Exception
    {
        userProfile.setPassword(passwordEncoder.encode(userProfile.getPassword()));
        databaseMovies.createUser(userProfile);
    }
}
