package org.polytech.fr.securite.controller;

import com.opencsv.exceptions.CsvException;
import org.polytech.fr.securite.entity.Movie;
import org.polytech.fr.securite.firebase.DatabaseMovies;
import org.polytech.fr.securite.repository.Movies_repository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/movie/v1")
public class MovieController {
    private  final DatabaseMovies databaseMovies ;

    public MovieController(DatabaseMovies databaseMovies) {
        this.databaseMovies = databaseMovies;
    }

    @GetMapping("/initialise")
    public void initFireBaseDatabase() throws IOException, CsvException {
        List<Movie> movies = Movies_repository.getAllMovies(149);
        databaseMovies.setTemps(movies);
    }


    @GetMapping("/all")
    public List<Movie> getAllMovies() throws IOException, CsvException {
        return this.databaseMovies.getTemps();
    }

    @GetMapping("/{username}/movies")
    public List<Movie> getMoviesByUserName(@PathVariable("username")String username) throws Exception {
        return  databaseMovies.getMoviesByUserName(username);

    }

    @PostMapping("/{username}/favorite")
    public ResponseEntity<String> addMovieToUserPlayList(@PathVariable("username")String username,@RequestBody Movie movie) throws Exception {
        return  ResponseEntity.ok(databaseMovies.addMovieToUserPlaylist(movie, username).getShowId() + "has been added successfully");
    }

    @PostMapping("/create")
    public ResponseEntity<String> createMovie(@RequestBody Movie movie)
    {
       return ResponseEntity.ok(databaseMovies.createMovie(movie));
    }

    @DeleteMapping("{id}")
    public void deleteMovieById(@PathVariable("id") String id)
    {
        databaseMovies.deleteMoviesById(id);
    }
}