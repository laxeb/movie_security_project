package org.polytech.fr.securite.repository;


import com.opencsv.exceptions.CsvException;
import org.polytech.fr.securite.entity.Movie;
import org.polytech.fr.securite.utils.Parseur;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class Movies_repository {

    // nom du fichier csv qui nous servira de base de données
    private final static String DBNAME = "src/main/java/org/polytech/fr/securite/repository/netflix_titles.csv" ;

    public static Movie getMovieById(String id) throws IOException, CsvException {
        String fileId = "src/main/java/org/polytech/fr/securite/repository/netflix_titles.csv";
       Movie movie =  Parseur.parseMovies(fileId).stream()
                                    .filter(x -> x.getShowId().equals(id))
                                    .findFirst().get();
       return movie ;
    }


    public static List<Movie> getAllMovies(long num) throws IOException, CsvException {
        String fileId = "src/main/java/org/polytech/fr/securite/repository/netflix_titles.csv";
        List<Movie> movieId =  Parseur.parseMovies(fileId).stream()
                .limit(num)
                .collect(Collectors.toList());

        return  movieId;
    }

    public static Movie getMovieByTitle(String title) throws IOException, CsvException {
        String fileId = "src/main/java/org/polytech/fr/securite/repository/netflix_titles.csv";
        Movie movie =  Parseur.parseMovies(fileId).stream()
                .filter(x -> x.getTitle().equals(title))
                .findFirst().get();
        return movie;
    }

    public static List<Movie> getMoviesByDirector(String director) throws IOException, CsvException {
        String fileId = "src/main/java/org/polytech/fr/securite/repository/netflix_titles.csv";
        List<Movie> movies;
        movies  =  Parseur.parseMovies(fileId).stream()
                .filter(x -> x.getDirector().contains(director))
                .toList();
        return movies;
    }

    public static String getDbName()
    {
        System.out.println("mouna");
        return DBNAME ;

    }
}
