package org.polytech.fr.securite;

import com.opencsv.exceptions.CsvException;
import org.polytech.fr.securite.config.RsaKeyProperties;
import org.polytech.fr.securite.entity.Movie;
import org.polytech.fr.securite.firebase.DatabaseMovies;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.polytech.fr.securite.utils.Parseur;
import org.polytech.fr.securite.repository.Movies_repository ;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import  java.util.Map ;
@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class SecuriteApplication {

	public static void main(String[] args) throws IOException, CsvException {
		SpringApplication.run(SecuriteApplication.class, args);
	}

}
