package org.polytech.fr.securite.firebase;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import  com.google.firebase.database.DatabaseReference ;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import lombok.extern.slf4j.Slf4j;
import org.polytech.fr.securite.config.ArrayUserDetailServices;
import org.polytech.fr.securite.entity.Movie;
import org.polytech.fr.securite.entity.UserProfile;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.FileInputStream;
import java.io.IOException;

import java.util.HashMap;
import java.util.List;

import static java.lang.System.*;


@Service
@Slf4j
public class DatabaseMovies {

    private final String databaseUrl = "https://movies-security-default-rtdb.firebaseio.com";

    private DatabaseReference database;

    private HashMap<String, Movie> movies;

    private HashMap<String, UserProfile> userProfileHashMap;

    public DatabaseMovies() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("src/main/java/org/polytech/fr/securite/firebase/movies-security-firebase-adminsdk-du72e-6b9d3d6c5f.json");
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl(databaseUrl)
                .build();
        FirebaseApp.initializeApp(options);
        database = FirebaseDatabase.getInstance().getReference();

        DatabaseReference movieRef = database.child("movies");
        movieRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Movie> temps = new HashMap<>(155);
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Movie movie = data.getValue(Movie.class);
                    if (temps.size() < 150) {
                        temps.putIfAbsent(movie.getShowId(), movie);
                    }
                    if (temps.containsKey(movie.getShowId())) {
                        temps.computeIfPresent(movie.getShowId(), ((s, movie1) -> movie1 = movie));
                    }
                }
                movies = temps;
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

        DatabaseReference userProfRef= database.child("users-profiles");
        userProfRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, UserProfile> userProfileHashMaptemp = new HashMap<>();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    out.println((data));
                    UserProfile user = data.getValue(UserProfile.class);
                    userProfileHashMaptemp.putIfAbsent(user.getEmail(), user);
                    if (ArrayUserDetailServices.users.stream().noneMatch(x -> x.getUsername().equals(user.getUsername())))
                        ArrayUserDetailServices.addUser(user);
                }
                userProfileHashMap = userProfileHashMaptemp;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


            }
        });

        out.println((userProfileHashMap));

    }

    public List<Movie> getTemps() {
        return movies.values().stream().toList();
    }

    public void setTemps(List<Movie> temps) {

        DatabaseReference movieRef = database.child("movies");

        for (Movie mv : temps) {
            movieRef.child(mv.getShowId()).setValueAsync(mv);
        }
    }

    public String createMovie(Movie movie)
    {
        DatabaseReference movieRef = database.child("movies");
        movieRef.child(movie.getShowId()).setValueAsync(movie);
        return movie.getShowId();

    }

    public void deleteMoviesById(String id) {
        database.child("movies").child(id).removeValue((databaseError, databaseReference) -> {
        });
    }

    public void createUser(UserProfile userProfile) throws Exception {
        if(ArrayUserDetailServices.users.stream().anyMatch(x->x.getUsername().equals(userProfile.getEmail())))
        {
            throw new Exception("this email already exist in our database");
        }
        savedUser(userProfile);
    }

    public List<Movie> getMoviesByUserName(@PathVariable("username") String username) throws Exception {
        UserProfile profile  = userProfileHashMap.get(username) ;
        if( profile == null )
        {
            throw new Exception("cannot find the user");
        }
        return profile.getPlaylist();
    }

    public Movie addMovieToUserPlaylist(Movie movie, String username) throws Exception {
        UserProfile userProfile = userProfileHashMap.get(username);
        if (userProfile == null) {
            throw new Exception("user profile not found");
        }
        userProfile.addMovieToPlayList(movie);
        database.child("users-profiles").orderByChild("username").equalTo(username).limitToFirst(1).getRef().child(userProfile.getId()).setValueAsync(userProfile);
        return movie;
    }
    public void savedUser(UserProfile userProfile)
    {
        DatabaseReference userProfRef = database.child("users-profiles");
        userProfRef.child(userProfile.getId()).setValueAsync(userProfile);
    }
}