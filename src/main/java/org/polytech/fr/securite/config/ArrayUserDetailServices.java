package org.polytech.fr.securite.config;

import org.polytech.fr.securite.entity.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

import static java.lang.System.out;

@Service
public class ArrayUserDetailServices implements UserDetailsService {
    public static Set<UserDetails> users = new HashSet<>();


    public static void addUser(UserProfile userProfile )
    {

        users.add(User.withUsername(userProfile.getEmail())
                .password(userProfile.getPassword())
                .authorities("read")
                .build());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        out.println("loading users" + username);
        return users.stream()
                .filter(user -> user.getUsername().equals(username))
                .findFirst()
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

}
