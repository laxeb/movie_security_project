package org.polytech.fr.securite.entity;

import lombok.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class Movie {

    private static int number = 0 ;

    private String id ;

    @NonNull
    private String showId ="default"  + number;

    @NonNull
    private String type ;

    @NonNull
    private String title;

    private List <String> director ;

    private List <String> cast  ;

    private String  country;

    // format September 24, 2021
    private String dateAdded;

    // format September 24, 2021
    private String  releaseYear;


    private String rating;

    // format exemple 125 min, 2 Seasons
    private String duration ;

    private List<String> listedIn ;

    private String description ;

    public Movie(){
        number ++ ;
    }

}
