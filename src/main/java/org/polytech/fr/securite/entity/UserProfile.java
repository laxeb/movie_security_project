package org.polytech.fr.securite.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class UserProfile {
    private static int number = 0 ;
    private String id ;
    private String email ;
    private String username ;
    private String password ;
    private List<Movie> playlist = new ArrayList<>() ;

    public  UserProfile(){
        number ++ ;
        id = "profile" + number;
    }



    public void addMovieToPlayList(Movie movie)
    {
        playlist.add(movie);
    }
}
