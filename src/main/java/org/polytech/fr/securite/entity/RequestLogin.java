package org.polytech.fr.securite.entity;

public record RequestLogin(String username , String password) {
}
