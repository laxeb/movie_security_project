import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {  Observable } from 'rxjs';
import appConstant from '../constant/app.constant';
import { Movies } from '../interfaces/movies.interface';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor( private http : HttpClient) { }

   getAllMovies(): Observable<Movies[]>{
      return this.http.get<Movies[]>(`${appConstant.BACKEND}/${appConstant.MOVIE}/all`);
   }

   getMoviesById(id : string)
   {
    return this.http.get<Movies[]>(`${appConstant.BACKEND}/${appConstant.MOVIE}/${id}/movies`);
   }

   deleteById(id :string)
   {
    return this.http.delete<any>(`${appConstant.BACKEND}/${appConstant.MOVIE}/${id}`);
   }

   addToMyFavorite(movie : Movies)
   {
    const username = localStorage.getItem("USERNAME");
    return this.http.post(`${appConstant.BACKEND}/${appConstant.MOVIE}/${username}/favorite`,movie,{ responseType: 'text'});
   }
   saveMovie(movie : Movies)
   {
    return this.http.post(`${appConstant.BACKEND}/${appConstant.MOVIE}/create`,movie,{ responseType: 'text'});
   }


   
}
