
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, mapTo, Observable, of, tap } from 'rxjs';
import appConstant from '../constant/app.constant';
import { Users } from '../interfaces/Users';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Define the token variable

  private readonly JWT_TOKEN = 'JWT_TOKEN';

  private readonly USERNAME = 'USERNAME';

  private loggedUser! : string ; 

  constructor( private http : HttpClient) {
    
   }


  // 
  login(user: { username: string, password: string }): Observable<boolean> {
    const url = `${appConstant.BACKEND}/${appConstant.TOKEN_ROUTE}`
    
    return this.http.post(url, user , { responseType: 'text'})
      .pipe(
        tap(token => this.doLoginUser(user.username, token)),
        mapTo(true),
        catchError(error => {
          alert(error.error);
          return of(false);
        }));
  }

  //TODO add logout to remove make the user token unavable so he can't use it anymore 


  private doLoginUser(username: string, token: string) {
    this.loggedUser = username;
    this.storeJwtToken(token);
    this.storeUserName(username) ;
  }


  // check if the users is login 
  isLoggedIn() {
    return !!this.getJwtToken();
  }

// log out the user while he logout 
  private doLogoutUser() {

    this.loggedUser = '';
    this.removeTokens();
  }

  // get the token from localStorage 
  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN) || '';
  }

  // store the toke in the localstorage 
  private storeJwtToken(jwt: string) {
    // to avoid storing a lot of tokens we remove all the existing first
    this.removeTokens();
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }
    // store the toke in the localstorage 
    private storeUserName(username: string) {
      localStorage.setItem(this.USERNAME, username);
    }

// remove the token in the localstorage
  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
  }

  public register(user : Users){
    const url = `${appConstant.BACKEND}/${appConstant.REGISTER}`
    console.log(url);
    localStorage.clear();
    console.log(user);
    return this.http.post(url, user); 
  }

}
