const appConstant = {
    BACKEND :"http://localhost:8080" ,
    TOKEN_ROUTE :"authenticate/token",
    MOVIE : "movie/v1",
    REGISTER : "authenticate/add/user"
} ;

export default appConstant ; 