import { Component, EventEmitter, OnInit} from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { MatDialog} from '@angular/material/dialog';
import { AddDialogComponent } from 'src/app/components/add-dialog/add-dialog.component';
import { Movies } from 'src/app/interfaces/movies.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  
  ngOnInit(): void {
       //this.movieService.getAllMovies().subscribe(
       // data => console.log(data) 
//
       //)
  
}
  
  constructor(private movieService : MoviesService, private dialogRef: MatDialog)
  {
  
  }
  opendialog(){
    
      let dialogReff =  this.dialogRef.open(AddDialogComponent  , {
      width : '100%'
    }); 
    dialogReff.componentInstance.close.subscribe(()=>{
      this.dialogRef.closeAll();
    }) ; 

    dialogReff.componentInstance.save.subscribe((data : Movies)=>{
      this.movieService.saveMovie(data).subscribe({
        next: (val) => console.log(`${val} has been added successfully`),
        error:()=> alert("something went wrong while adding the movie" +data.showId )
      })
      this.dialogRef.closeAll();})
  }
  
 
  isBiggerScreen() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width < 768) {
      return true;
    } else {
      return false;
    }
  }

}
