export interface Movies {
    showId? : string , 
    title : string ,
    type : string , 
    directors : string[] ,
    description : string  , 
    rating ?: string , 
    dateAdded : string , 
    duration : string , 
}