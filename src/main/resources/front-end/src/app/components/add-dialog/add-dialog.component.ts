import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Movies } from 'src/app/interfaces/movies.interface';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.css']
})

export class AddDialogComponent implements OnInit {

  form = new FormGroup(
    {

    title : new FormControl('', [Validators.required, Validators.maxLength(50)]),  

    type :new FormControl('', [Validators.required]),
  
    directors: new FormControl([], Validators.required),
  
    description :new FormControl('', Validators.required),
  
    duration: new FormControl("", Validators.required),
     
    dateAdded : new FormControl("",Validators.required),
  });

  @Output() close: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<any> = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  isFormValid()
  {
    return this.form.valid ;
  }
  
  onSave()
  {
      const movie = { ...this.form.value , userId :localStorage.getItem("USERNAME")} as Movies; 
      this.save.emit(movie); 
  }

  // When the user clicks the action button a.k.a. the logout button in the\
  // modal, show an alert and followed by the closing of the modal
  onCancel()
  {
    this.close.emit(null);
  }

}