import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Movies } from 'src/app/interfaces/movies.interface';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit , AfterViewInit  {
  public displayedColumns = ['title', 'type', 'description', 'rating','dateAdded','duration','Play'];
  public dataSource = new MatTableDataSource <Movies>();

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private movieService : MoviesService){}

  getAllMovie()
  {
    const username = localStorage.getItem("USERNAME") || ''; 
    this.movieService.getMoviesById(username).subscribe({
      next: mov =>this.dataSource.data = mov,
      error: err => console.log("can't fetch the movies because of "+ err),
      complete:()=>console.log("getting the movies completed")
    })

  }
  
  ngOnInit() {
    this.getAllMovie();
    console.log(this.dataSource.data)
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }  

 public redirectToPlay = (id: string) => {
  console.log(`adding that this movies is played ${id}`);
    
 }
 public redirectToAddToFavorite = (movie:Movies) => {
  this.movieService.addToMyFavorite(movie).subscribe({
    next: val => console.log(val),
    error: ()=> console.error("something went wrong")
  });
    
 }
 public redirectToDelete = (id: string) => {
  console.log(`deleting this movies ${id}`);
  this.movieService.deleteById(id).subscribe(
  {
    next : (val) =>{ console.log("movie delete successfuly" +id);
    this.getAllMovie();
  }
  }
  )
   
 }
 public doFilter = (event: Event) => {
  const value = (event.target as HTMLInputElement)?.value;
  this.dataSource.filter = value.trim().toLocaleLowerCase();
}

}
