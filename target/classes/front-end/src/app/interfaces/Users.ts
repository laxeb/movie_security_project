import { Movies } from "./movies.interface";

export interface Users {
    username : string ;
    password : string ; 
    email ?  : string ; 
    jwtToken ?: string ;
    playlist ?: Movies[];
}