import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogInComponent } from './components/log-in/log-in.component';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './services/auth.guard';
import { AddDialogComponent } from './components/add-dialog/add-dialog.component';
import { MoviesComponent } from './components/movies/movie/movies/movies.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { FavoriteComponent } from './components/favorite/favorite.component';

const routes: Routes = [ 
  {path: '',   redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LogInComponent},
  {path: 'register', component: RegisterPageComponent},
  {path: 'home', component: HomeComponent , canActivate :[AuthGuard],
  children :[
    {path: '',   redirectTo: 'movies', pathMatch: 'full'},
    { 
      path: 'movies', 
      component: MoviesComponent
    },
    {
      path: 'addFilm', 
      component: AddDialogComponent, 
     },
     {
      path: 'favorite' , component : FavoriteComponent,

     }
  ]
  },
  
  //{path: 'home', component: HomeComponent ,canActivate:[AuthGuard]}
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
