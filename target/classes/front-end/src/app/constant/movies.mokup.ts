import { Movies } from "../interfaces/movies.interface";

const movies: Movies[] = [
    {
      showId: "1",
      title: "The Matrix",
      type: "movie",
      directors: ["Lana Wachowski", "Lilly Wachowski"],
      description:
        "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.",
      rating: "R",
      dateAdded: "2022-03-15",
      duration: "2h 16m",
    },
    {
      showId: "2",
      title: "Breaking Bad",
      type: "tv show",
      directors: ["Vince Gilligan"],
      description:
        "A high school chemistry teacher diagnosed with inoperable lung cancer turns to manufacturing and selling methamphetamine in order to secure his family's future.",
      rating: "TV-MA",
      dateAdded: "2022-03-16",
      duration: "5 seasons",
    },
    {
      showId: "3",
      title: "The Shawshank Redemption",
      type: "movie",
      directors: ["Frank Darabont"],
      description:
        "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
      rating: "R",
      dateAdded: "2022-03-17",
      duration: "2h 22m",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
    {
      showId: "4",
      title: "Stranger Things",
      type: "tv show",
      directors: ["The Duffer Brothers"],
      description:
        "When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.",
      rating: "TV-14",
      dateAdded: "2022-03-18",
      duration: "4 seasons",
    },
  ];

  export default movies ; 
  