import { AuthService } from 'src/app/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})


export class RegisterPageComponent implements OnInit {

  registerForm!: FormGroup;
  fieldRequired: string = "This field is required"
 
  constructor(private  authService: AuthService , private router : Router) {}

  ngOnInit() {
    this.registerForm = new FormGroup({
      username: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }


  /***public onSubmit() {
    this.authService.register(
      this.registerForm.get('username')!.value,
      this.registerForm.get('email')!.value,
      this.registerForm!.get('password')!.value
    );
  }***/

  public onSubmit(formData: FormGroup, formDirective: FormGroupDirective): void {
   
    localStorage.clear();
    const email = formData.value.email;
    const password = formData.value.password;
    const username = formData.value.username;
    let userToAdd = {
       username : username , 
       password : password , 
       email : email ,
    }

    this.authService.register(userToAdd)
                    .subscribe({
                      next: (val )=> {
                        console.log(val);
                        this.router.navigate(['/login'])},
                       error: err => alert("something went wrong" + err), 
                       complete : ()=>{
                        formDirective.resetForm();
                         this.registerForm.reset();
                      }
                    }
    );
     
}
  
}