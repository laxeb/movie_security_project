import { Component } from '@angular/core';
import { FormGroup ,Validators ,FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { Users } from 'src/app/interfaces/Users';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent {
  
  loginForm: FormGroup;

  constructor(private router: Router , private authService : AuthService, private formBuilder: FormBuilder) {
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required]],
        password: ['', Validators.required],
        rememberMe: [false]
      });
    }

  onSubmit() {
      if (this.loginForm.valid) {
        let user = { username : this.loginForm.get('email')?.value , 
                     password : this.loginForm.get('password')?.value} as Users; 
        localStorage.clear();
        this.authService.login(user).subscribe(
          result => { if (result === true)
          { this.router.navigate(['home']); 
          console.log(result)
        } }
        );
      }
  }

}
