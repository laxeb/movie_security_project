# Use a base image with Java 17 and Maven installed
FROM maven:3.8.3-openjdk-17-slim

# Set the working directory in the container
WORKDIR /app

# Copy the source code from the host to the container
COPY . .

# Build the application using Maven
RUN mvn package

# Expose port 8080 for the container
EXPOSE 8080

# Run the Spring Boot application when the container starts
CMD ["java", "-jar", "target/securite-0.0.1-SNAPSHOT.jar"]
